const express = require('express');

const app = express();
const port = process.env.PORT || 3000; // Use environment variable or default port

app.get('/', (req, res) => {
  res.send('Heello from Node.js app in Docker container!');
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
